class MyClass{
    constructor () {
        console.log("initiate");
    }

    sayHello(){
        console.log("Hello");
    }

    add(a, b){
        var result;
        result = a + b;
        return result;
    }

    callAnotherFn(a, b){
        this.sayHello();
        var result = this.add(a, b);
        return result;
    }
}

module.exports = MyClass;