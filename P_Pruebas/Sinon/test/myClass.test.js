var MyClass = require("../src/myClass");
var myObj = new MyClass();
var chai = require("chai");
var exports = chai.expect;
var sinon = require("sinon");
const { expect } = require("chai");

describe("Test suit", function() {
    it("Mock the sayHello method", function() {
        //Creamos un mock(simulacro) para el objeto
        var mock = sinon.mock(myObj);
        //Anulamos el metodo sayHello
        var expetcation = mock.expects("sayHello");
        // Llamamos al metodo exactamente n cantidad de veces
        expetcation.exactly(1);
        myObj.callAnotherFn(10, 20);
        //Verificamos todas las expectativas del mock
        mock.verify();
    });
});

describe("Test suit", function() {
    it("Stub the add method", function() {
        //Remplazamos el objeto y el metodo con una funcion de codigo auxiliar
        var stub = sinon.stub(myObj, "add");
        //Actua de manera diferente a los resultados de los argumentos
        //Con returns devolvemos el valor que proporcionamos
        stub.withArgs(10, 20).returns(100);
        expect(myObj.callAnotherFn(10, 20)).to.be.equal(100);
    });
});