const assert = require('assert');
const Calculadora = require('../models/Calculadora');

describe('Funciones de una Calculadora', function() {
    it("Funcion Exponencial", function() {
        let num = 5;
        let exponente = 2;
        let calculadora = new Calculadora();
        let resultado = calculadora.exponencial(num, exponente);

        assert.strictEqual(25, resultado);
    })
})

