const getAboutUsLink = require("../models/idioma");

test("Devuelve about-us para el idioma Ingles", () => {
    expect(getAboutUsLink("en-US")).toBe("/about-us");
});