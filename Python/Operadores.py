# Operadores Aritméticos
num1 = 10
num2 = 3

suma = num1 + num2
resta = num1 - num2
multiplicacion = num1 * num2
division1 = num1 / num2
division2 = num1 // num2
residuo = num1 % num2
exponente = num1 ** num2

''' Ejercicio 1'''
res = 3**3 * (13/5 - (2*4))

print(res)

# Operadores Relacionales
x = 12
y = 8

menor = x < y
mayor = x > y
igualdad = x == y
diferente = x != y
menor_igual = x <= y
mayor_igual = x >= y

''' Ejercicio 2 '''
n1 = 10
n2 = 20
n3 = 30

res1 = n1+n2 == n3

print(res1)

# Operadores Lógicos
''' 
OR = Suma Lógica
AND = Multiplicación Lógica
NOT = Negación
'''
a = 10
b = 12
c = 13

res2 = ((a>b)or(a<c))and((a==c)or(a>=b))
res3 = not(res2)
print(res2)
print(res3)

# Operadores de Asignación
i = 0

i += 5 # Suma en Asignación
i -= 2 # Resta en Asignación
i *= 4 # Multiplicación en Asignación
i /= 3 # División en Asignación
i **= 2 # Potencia en Asignación
i %= 2 # Modulo en Asignación

print(i)