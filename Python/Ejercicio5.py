# Descuento del 15% en el total de una compra 
precio = float(input("Digite el precio del producto: "))

descuento = precio * 0.15
total = precio - descuento

print(f"El precio total del producto es: ${total:.2f}")