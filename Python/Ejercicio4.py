# Area y Perimetro (Longitud) de un Circulo
import math
r = float(input("Ingresa el radio del circulo: "))

area = math.pi * r**2
longitud = 2 * math.pi * r

print(f"El area del circulo es: {area:.3f}")
print(f"La longitud del circulo es: {longitud:.2f}")