# Intercambiar Valores

a = input("a = ")
b = input("b = ")

#forma 1 
a , b = b , a

print(f"El nuevo valor de 'a' es: {a}")
print(f"El nuevo valor de 'b' es: {b}")

#forma2
'''
c = 0

c = a
a = b
b = c
'''
