# Salidas de Datos

nombre = "Alberto"
edad = 22

#forma 1
print('Hola',nombre,'tienes',edad,'años')
#forma 2
print("Hola {} tienes {} años".format(nombre,edad))
#forma 3
print(f"Hola {nombre} tienes {edad} años")