/* Tipo String */
let nombre = 'Alberto';

/* Tipo Number */
const peso = 70;

/* Tipo Boolean */
let estudia = true;

/* Tipo Null */
var foo = null;

/* Tipo Undefined */
var trabaja;

/* Tipo Symbol */
let sym = Symbol('foo');

/* Tipo Object Forma 1 */
var user1 = {nombre: "Alberto", edad: "22", ciudad: "Playas de Rosarito"};

/* Tipo Object Forma 2 */
var user2 = {
    nombre: "Alberto",
    edad: "22",
    ciudad: "Playas de Rosarito",
  };
