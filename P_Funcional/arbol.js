class Checklist {
    constructor(groups = []) {
        this.groups = groups;
    }

    add(group) {
        
    }
}

class SubGroup {
    constructor(title, subGroups = [], checks = []) {
        this.title = title;
        this.subGroups = subGroups;
        this.checks = checks;
    }
    addSubGroup(subGroup){
        this.subGroups.push(subGroup);
    }
}

class Group {
    constructor (title, subGroups = [], checks = []) {
        this.title = title;
        this.subGroup = subGroups;
        this.checks = checks;
    }
    add(subGroup){
        this.subGroup.push(subGroup);
    }
    add(check){
        this.checks.push(check);
    }

}

class Check {
    constructor(name, value = 0) {
        this.name = name;
        this.value = value;
    }
}
