// let = declarar variables limitando su alcance o en otras palabras variables propias
// var = declara variables globales o locales sin importar el ambito de bloqueo

//Dato tipo Numero
let num = 7.7;
console.log(num);
//Dato tipo Cadena
let cadena = 'Esto es un String';
console.log(cadena);
//Dato tipo Boolean
let bool = true;
console.log(bool);
//Dato tipo Arreglo
var arreglo = [1, 3, 4, 5];
console.log(arreglo);
//Dato tipo Objeto
var  obj = {"Nick": 'Ryu', "Rango": 'Plata 2', "Juego": 'Valorant'};
console.log(obj);

// const = Dato tipo constantes
const PI = 3.1416;
console.log(PI);